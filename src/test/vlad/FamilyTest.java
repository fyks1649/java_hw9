package vlad;

import org.hamcrest.core.Is;
import org.junit.*;

import java.util.Arrays;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

public class FamilyTest {
    private Human mother1 = new Human("Elena", "Lietun");
    private Human father1 = new Human("Vlad", "Lietun");

    private Family family1 = new Family(mother1, father1);
    private Human child1 = new Human("Nick", "Lietun");
    private Human child2 = new Human("Denis", "Lietun");

    private Human mother2 = new Human("Kate", "Bubalo");
    private Human father2 = new Human("Nick", "Bubalo");
    private Family family2 = new Family(mother2, father2);

    @Before
    public void setUp() {
        family1.addChild(child1);
        family1.addChild(child2);
    }

    public String getExpectedToString(Family family) {
        String result = "Family{mother=" + family.getMother() +
                ", father=" + family.getFather() +
                ", children=" + Arrays.toString(family.getChildren()) + ", pet=null}";
        return result;
    }

    @Test
    public void testToString(){
        Assert.assertThat(family1.toString(), Is.is(equalTo(getExpectedToString(family1))));
    }

    @Test
    public void testDeleteChildPositive() {
        int resultBefore = family1.getChildren().length;
        family1.deleteChild(child2);
        Assert.assertThat(family1.getChildren().length, Is.is(equalTo(resultBefore-1)));

        for (Human child : family1.getChildren()) {
            Assert.assertNotSame(child, child2);
        }
    }

    @Test
    public void testDeleteChildNegative() {
        String resultBefore = Arrays.toString(family1.getChildren());
        family1.deleteChild(new Human("Nick", "Lietu"));
        Assert.assertEquals(resultBefore, Arrays.toString(family1.getChildren()));
    }

    @Test
    public void testDeleteChildIndexPositive() {
        int resultBefore = family1.getChildren().length;
        family1.deleteChild(1);
        Assert.assertThat(family1.getChildren().length, Is.is(equalTo(resultBefore-1)));

        for (Human child : family1.getChildren()) {
            Assert.assertNotSame(child, child2);
        }
        Assert.assertThat(Arrays.toString(family1.getChildren()), Is.is(equalTo("[" + child1.toString() + "]")));
    }

    @Test
    public void testDeleteChildIndexNegative() {
        int resultBefore = family1.getChildren().length;
        int index = 0;
        if (index > family1.getChildren().length-1) {
            family1.deleteChild(index);
            Assert.assertThat(family1.getChildren().length, Is.is(equalTo(resultBefore-1)));

            for (Human child : family1.getChildren()) {
                Assert.assertNotSame(child, child2);
            }
            Assert.assertThat(Arrays.toString(family1.getChildren()), Is.is(equalTo("[" + child1.toString() + "]")));
        }
    }

    @Test
    public void testAddChildPositive() {
        int resultBefore = family1.getChildren().length;
        Human newChild = new Human("Pavel", "Lietun");
        family1.addChild(newChild);
        Assert.assertThat(family1.getChildren().length, Is.is(equalTo(resultBefore+1)));
        Assert.assertEquals(family1.getChildren()[family1.getChildren().length-1], newChild);
    }

    @Test
    public void testCountFamily() {
        int resultCount = family2.countFamily();
        family2.addChild(new Human());
        Assert.assertEquals(++resultCount, family2.countFamily());
        family2.addChild(new Human());
        Assert.assertEquals(++resultCount, family2.countFamily());
    }
}
