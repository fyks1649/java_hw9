package vlad;

import org.hamcrest.core.Is;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.*;

import java.util.Arrays;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.*;

public class HumanTest {
    private Human human = new Human("Vlad", "Lietun", 1995, 100, new String[][]{{String.valueOf(DayOfWeek.MONDAY)}, {"To do smth"}});
    private Human human1 = new Human("Denis", "Lietun", 1995, 100, new String[][]{{String.valueOf(DayOfWeek.MONDAY)}, {"To do smth"}});

    public String getExpectedToString(Human human) {
        String result = "Human{name=" + human.getName() +
                ", surname=" + human.getSurname() +
                ", year=" + human.getYear() +
                ", iq=" + human.getIq() +
                ", schedule=" + Arrays.deepToString(human.getSchedule()) + "}";
        return result;
    }

    @Test
    public void testToString(){
        Assert.assertThat(human.toString(), Is.is(equalTo(getExpectedToString(human))));
    }
}