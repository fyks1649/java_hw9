package vlad;

import org.hamcrest.core.Is;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.*;

public class PetTest {
    private Pet pet = new Pet(Species.DOG, "Tuzyk", 5, 50, new String[]{"layat"});
    private Pet pet1 = new Pet(Species.CAT, "Tuzyk", 5, 50, new String[]{"layat"});

    public String getExpectedToString(Pet pet) {
        String result = pet.getSpecies() +
                "{nickname='" + pet.getNickname() +
                "', age=" + pet.getAge() +
                ", trickLevel=" + pet.getTrickLevel() +
                ", habits=" + Arrays.toString(pet.getHabits()) + "}";
        return result;
    }

    @Test
    public void testToString(){
        Assert.assertThat(pet.toString(), Is.is(equalTo(getExpectedToString(pet))));
    }
}